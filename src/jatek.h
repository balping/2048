/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fxcg_syscalls.h>
typedef char bool;
#define false 0
#define true 1

void dialog_continue();
void dialog_vege();

typedef struct{
	char ures_mezok;
	int score;
	int highscore;
	int moves;
	bool reached; //2048 megvan-e
	unsigned char palya[4][4];
} t_jatek;

t_jatek jatek;

unsigned int lastrandom;
unsigned int random(unsigned int seed){
	if (seed) lastrandom=seed;
	lastrandom = ( 0x41C64E6D*lastrandom ) + 0x3039;
	return lastrandom;
}

void init_jatek(){
	jatek.ures_mezok = 16;
	jatek.score = 0;
	jatek.moves = 0;
	if(!van_highscore){jatek.highscore = 0; van_highscore = true;}
	jatek.reached = 0;
	int x, y;
	for(x=0; x<4; x++){
	for(y=0; y<4; y++){
		jatek.palya[x][y] = 0;
	}
	}
	
	random(RTC_GetTicks());
	
	add_tile(); add_tile();
}

void add_tile(){
	if(jatek.ures_mezok>0){
		int rand = random(0)%jatek.ures_mezok;
		int x, y, i;
		i=0;
		for(x=0; x<4; x++){
		for(y=0; y<4; y++){
			if(jatek.palya[x][y] == 0){
				if(i==rand){
					jatek.ures_mezok--;
					jatek.palya[x][y] = random(0)%10 == 0 ? 2 : 1;
					return;
				}
				i++;
			}
		}
		}
	}
}

int hatvany(int i){ //2 hatványok
	return ((int)1) << i;
}

void jatek_ellenoriz(){
	int x, y;
	if(!jatek.reached){//2048 ellenőrzése
		for(x=0; x<4; x++){
		for(y=0; y<4; y++){
			if(jatek.palya[x][y] == 11){
				x=4; y=4;
				jatek.reached = true;
				dialog_continue();
				save_game();
			}
		}
		}
	}
	
	char elozo;
	if(jatek.ures_mezok==0){ //vége ellenőrzése
		for(x=0; x<4; x++){
			elozo = jatek.palya[x][0];
			for(y=1; y<4; y++){
				if(jatek.palya[x][y] == elozo){
					return;
				}else{
					elozo = jatek.palya[x][y];
				}
			}
		}
		
		for(y=0; y<4; y++){
			elozo = jatek.palya[0][y];
			for(x=1; x<4; x++){
				if(jatek.palya[x][y] == elozo){
					return;
				}else{
					elozo = jatek.palya[x][y];
				}
			}
		}
		dialog_vege();
		save_game();
	}
	
	
}

void lep(char irany){//0 fel, 1: jobbra, 2: le, 3: balra
	int x, y;
	int also;
	bool tort = 0;
	
	
	switch(irany){
		case 0:
			for(x=0; x<4; x++){
				also = 0;
				for(y=1; y<4; y++){
					if(also!=y){
						if(jatek.palya[x][y] > 0){
							if(jatek.palya[x][y] == jatek.palya[x][also]){
								jatek.palya[x][y] = 0;
								jatek.ures_mezok++;
								jatek.palya[x][also]++;
								jatek.score += hatvany(jatek.palya[x][also]);
								also++;
								tort = 1;
							}else if(jatek.palya[x][also]==0){
								jatek.palya[x][also] = jatek.palya[x][y];
								jatek.palya[x][y] = 0;
								tort = 1;
							}else{
								also++;
								if(also!=y){
									jatek.palya[x][also] = jatek.palya[x][y];
									jatek.palya[x][y] = 0;
									tort = 1;
								}
							}
						}
					}
				}
			}
			
			break;
			
		case 2:
			for(x=0; x<4; x++){
				also = 3;
				for(y=3; y>=0; y--){
					if(also!=y){
						if(jatek.palya[x][y] > 0){
							if(jatek.palya[x][y] == jatek.palya[x][also]){
								jatek.palya[x][y] = 0;
								jatek.ures_mezok++;
								jatek.palya[x][also]++;
								jatek.score += hatvany(jatek.palya[x][also]);
								also--;
								tort = 1;
							}else if(jatek.palya[x][also]==0){
								jatek.palya[x][also] = jatek.palya[x][y];
								jatek.palya[x][y] = 0;
								tort = 1;
							}else{
								also--;
								if(also!=y){
									jatek.palya[x][also] = jatek.palya[x][y];
									jatek.palya[x][y] = 0;
									tort = 1;
								}
							}
						}
					}
				}
			}
			
			break;
		
		case 3:
			for(y=0; y<4; y++){
				also = 0;
				for(x=1; x<4; x++){
					if(also!=x){
						if(jatek.palya[x][y] > 0){
							if(jatek.palya[x][y] == jatek.palya[also][y]){
								jatek.palya[x][y] = 0;
								jatek.ures_mezok++;
								jatek.palya[also][y]++;
								jatek.score += hatvany(jatek.palya[also][y]);
								also++;
								tort = 1;
							}else if(jatek.palya[also][y]==0){
								jatek.palya[also][y] = jatek.palya[x][y];
								jatek.palya[x][y] = 0;
								tort = 1;
							}else{
								also++;
								if(also!=x){
									jatek.palya[also][y] = jatek.palya[x][y];
									jatek.palya[x][y] = 0;
									tort = 1;
								}
							}
						}
					}
				}
			}
			
			break;
			
		case 1:
			for(y=0; y<4; y++){
				also = 3;
				for(x=3; x>=0; x--){
					if(also!=x){
						if(jatek.palya[x][y] > 0){
							if(jatek.palya[x][y] == jatek.palya[also][y]){
								jatek.palya[x][y] = 0;
								jatek.ures_mezok++;
								jatek.palya[also][y]++;
								jatek.score += hatvany(jatek.palya[also][y]);
								also--;
								tort = 1;
							}else if(jatek.palya[also][y]==0){
								jatek.palya[also][y] = jatek.palya[x][y];
								jatek.palya[x][y] = 0;
								tort = 1;
							}else{
								also--;
								if(also!=x){
									jatek.palya[also][y] = jatek.palya[x][y];
									jatek.palya[x][y] = 0;
									tort = 1;
								}
							}
						}
					}
				}
			}
			
			break;
			
			
	}
	
	
	
	if(tort){
		add_tile();
		jatek.moves++;
		if(jatek.score>jatek.highscore){jatek.highscore = jatek.score;}
		save_game();
		jatek_ellenoriz();
	}
}


//char lep(char irany){//0 fel, 1: jobbra, 2: le, 3: balra
//	int x, y;
//	int also;
//	/*switch(irany){
//		case 0:
//			xstart = 0; start
//	}*/
//	
//	for(x=0; x<4; x++){
//		also = 0;
//		for(y=1; y<4; y++){
//			if(also!=y){
//				if(jatek.palya[x][y] > 0){
//					if(jatek.palya[x][y] == jatek.palya[x][also]){
//						jatek.palya[x][y] = 0;
//						jatek.ures_mezok++;
//						jatek.palya[x][also]++;
//						jatek.score += hatvany(jatek.palya[x][also]);
//						also++;
//					}else if(jatek.palya[x][also]==0){
//						jatek.palya[x][also] = jatek.palya[x][y];
//						jatek.palya[x][y] = 0;
//					}else{
//						also++;
//						if(also!=y){
//							jatek.palya[x][also] = jatek.palya[x][y];
//							jatek.palya[x][y] = 0;
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	add_tile();
//}
