/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SAROK_H
#define SAROK_H

unsigned short sarok[8][8] = {65501, 65501, 65501, 65501, 59162, 54839, 50613, 48500, 65501, 65501, 63388, 52726, 48500, 48500, 48500, 48500, 65501, 63388, 50613, 48500, 48500, 48500, 48500, 48500, 65501, 52726, 48500, 48500, 48500, 48500, 48500, 48500, 59162, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 54839, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 50613, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500, 48500};

#endif
