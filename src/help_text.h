/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const int help_sorok_szama=58;
const char help_text[] = "       2048 v1.00\n\nThis is a port of the po-\npular browser and mobile\ngame. Use your arrow keys\nto move the tiles. When\ntwo tiles with the same\nnumber touch, they merge\ninto one!\n\n\n      *** Files ***\n\nThis game saves one file\nto the main memory. You\ncan find it in\nMemory Manager/\n   Main Memory/\n      @2048/\n         GAME\n\nThis file contains your\ncurrent gameplay, and\nyour highscore. If you\ndelete it, you won't be\nable to continue your\npaused game, and your\nhighscore will be reset.\n\n\n       *** Bugs ***\n\nPlease note that this is\nthe first release of this\nprogram. Please send bug\nreports to my e-mail\naddress (below) or post\nit at the forum thread at\nChemetech\n( http://tiny.cc/7jjfex )\n\n\n     *** Credits ***\n\nProgrammed by Balping\nbalping.official@gmail.com\n(C) balping 2014\n\nOriginal game and design\nby Gabriele Cirulli\n   http://git.io/2048\n\nThis package is\ndownloadable at\ncemetech.net\nhttp://tiny.cc/8hjfex\n\n(Press [EXIT].)";
