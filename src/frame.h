/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define SCA 0xD201D002
#define SCB 0x422B0009
#define SCE 0x80020070

typedef unsigned short(*sc_usius)(int, unsigned short);

const unsigned int sc02A3[4] = { SCA, SCB, SCE, 0x02A3 };
#define FrameColor (*(sc_usius)sc02A3)
