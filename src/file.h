/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <MCS_syscalls.h>
#include "hash.h"

unsigned char dir[] = "@2048";
unsigned char file_game[] = "GAME";
unsigned short fejlec_hossz = 4+2+1; //hash + fullsize + version
unsigned short game_hossz = (1+4+4+4+1+4*4);

/*typedef struct{
	char ures_mezok;
	int score;
	int highscore;
	int moves;
	bool reached; //2048 megvan-e
	unsigned char palya[4][4];
} t_jatek;*/


unsigned int lastrandom=31415926;
unsigned char code_random(unsigned int seed){
	if (seed) lastrandom=seed;
	lastrandom = ( 0x41C64E6D*lastrandom ) + 0x3039;
	return ( (unsigned char)(lastrandom >> 16) );
}

void encode(unsigned char * buffer, unsigned short len){
	unsigned short i;
	unsigned int key2;
	memcpy(&key2, buffer, 4);
	code_random(key2);
	for(i=4; i<len; i++){
		buffer[i] ^= code_random(0);
	}
}

void save_game(){
	int size;
	unsigned short szukseges_size=game_hossz+fejlec_hossz;
	unsigned char buffer[szukseges_size];
	unsigned char *p = buffer+4;
	memcpy(p, &szukseges_size, 2); p+=2;
	*p = VERSION; p++;
	
	*p = jatek.ures_mezok; p++;
	memcpy(p, &jatek.score, 4); p+=4;
	memcpy(p, &jatek.highscore, 4); p+=4;
	memcpy(p, &jatek.moves, 4); p+=4;
	*p = jatek.reached; p++;
	memcpy(p, jatek.palya, 16); p+=16;

	
	__uint32_t hash = SuperFastHash(buffer+4, (int)szukseges_size-4);
	memcpy(buffer, &hash, sizeof(__uint32_t));
	encode(buffer, szukseges_size);

	if(MCSGetDlen2(dir, file_game, &size) != 0){
		//még nem létezik, létrehozzuk
		MCSPutVar2(dir, file_game, szukseges_size, (void*)buffer);
	}else{
		if(size<szukseges_size || size>szukseges_size+4){
			//kisebb, mint a szükséges, vagy jóval nagyobb; törlés, majd újra létrehozás
			MCSDelVar2(dir, file_game);
			MCSPutVar2(dir, file_game, szukseges_size, (void*)buffer);
		}else{
			MCSOvwDat2(dir, file_game, szukseges_size, (void*)buffer, 0); 
		}
	}
}

void load_game(){
	int size;
	unsigned short szukseges_size=game_hossz+fejlec_hossz;
	unsigned char buffer[szukseges_size];

	if(MCSGetDlen2(dir, file_game, &size) == 0){
		if(size>=szukseges_size){
			MCSGetData1(0, szukseges_size, (void*)buffer);
			encode(buffer, szukseges_size);
			
			__uint32_t hash = SuperFastHash(buffer+4, (int)szukseges_size-sizeof(__uint32_t));
			__uint32_t hash2;
			memcpy(&hash2, buffer, sizeof(__uint32_t));
			if(hash==hash2){
				unsigned char *p = buffer+fejlec_hossz;
				
				jatek.ures_mezok = *p; p++;
				memcpy(&jatek.score, p, 4); p+=4;
				memcpy(&jatek.highscore, p, 4); p+=4;
				memcpy(&jatek.moves, p, 4); p+=4;
				jatek.reached = *p; p++;
				memcpy(jatek.palya, p, 16); p+=16;
				
				firstrun = false;
				van_highscore = true;
			}
		}
	}
}
