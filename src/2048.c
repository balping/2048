/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define VERSION 100

#include <fxcg/display.h>
#include <color.h>
#include <display_syscalls.h>
#include <disp_tools.hpp>
#include <keyboard.h>
#include <keyboard.hpp>
#include <string.h>
#include <sprite.h>
#include <fxcg.h>

char firstrun = 1;
van_highscore = 0;

#include "jatek.h"
#include "display.h"
#include "file.h"
#include "help.h"



int main(void) {
	init_jatek();
	load_game();
	init_display();
	jatek_ellenoriz();
	display_grid();
	display_score();
	
	if(firstrun){help(1);}
	
	int key;
	int i;

	while (1) {
		GetKey(&key);
		switch(key){
			case KEY_CTRL_UP:
				lep(0);
				display_grid();
				display_score();
				break;
			case KEY_CTRL_DOWN:
				lep(2);
				display_grid();
				display_score();
				break;
			case KEY_CTRL_LEFT:
				lep(3);
				display_grid();
				display_score();
				break;
			case KEY_CTRL_RIGHT:
				lep(1);
				display_grid();
				display_score();
				break;
			case KEY_CTRL_F2:
				if((jatek.ures_mezok==14 && jatek.score==0) || new_game_dialog()){
					init_jatek();
					display_grid();
					clear_score();
					display_score();
					save_game();
				}
				break;
			case KEY_CTRL_F1:
				help(0);
				break;
		}
		
		
		
	}
	return 0;
}
