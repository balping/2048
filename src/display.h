/*
    2048 for CASIO PRIZM calculators
    Copyright (C) 2014  Balázs Dura-Kovács
    Original game and design by Gabriele Cirulli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <sprite.h>
#include "sprites.h"
#include "sarok.h"
#include "quicklz.h"
#include "frame.h"

#define TABLA_X 0
#define TABLA_Y 25

#define SZIN_HATTER 0xFFDD
#define SZIN_TABLA 0xBD74
#define SZIN_GOMB_HATTER 0x8BCC
#define SZIN_GOMB_FELIRAT 0xFFBE


#define malloc sys_malloc
#define free sys_free
//#define memcpy sys_memcpy

unsigned short tiles[18][1600] = {};

int gomb(int cx, int cy, char * felirat, char draw, char tipus){//return width ; tipus: 0: gomb, 1: score
	int padding = 8;
	int font_height = 18;
	int x=cx+padding;
	int y=cy+padding;
	int dx = 0;
	PrintMini(&dx, &y, felirat, 0x40, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 0, 0);
	
	if(draw){
		unsigned short szin, hatszin;
		if(tipus==0){
			szin = SZIN_GOMB_FELIRAT; hatszin = SZIN_GOMB_HATTER;
		}else{
			szin = COLOR_WHITE; hatszin = 0xBD74;
		}
		fillArea(cx, cy, 2*padding+dx, 2*padding+font_height, hatszin);
		PrintMini(&x, &y, felirat, 0x40, 0xFFFFFFFF, 0, 0, szin, hatszin, 1, 0);
	}
	
	return 2*padding+dx;
}

void gomb_kozepre(int balszel, int jobbszel, int cy, char * felirat, char tipus){
	int x = jobbszel - balszel;
	x = (x-gomb(0, 0, felirat, 0, 0))/2;
	x += balszel;
	gomb(x, cy, felirat, 1, tipus);
}

void clear_score(){
	fillArea(TABLA_X+190, TABLA_Y, LCD_WIDTH_PX-TABLA_X-190,(18+2*8)*2+3, SZIN_HATTER);
}

void display_score(){
	DisplayStatusArea();
	char buffer[] = "Score: 3932156";
	itoa(jatek.score, buffer+7);
	char buffer2[] = "Best: 3932156";
	itoa(jatek.highscore, buffer2+6);
	char buffmoves[] = "Moves: #######";
	itoa(jatek.moves, buffmoves+7);
	int x=375;
	int y=3;
	int dx = 0;
	PrintMini(&dx, &y, buffmoves, 0x40, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 0, 0);
	x -= dx;
	PrintMini(&x, &y, buffmoves, 0x40, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 1, 0);
	
	gomb_kozepre(TABLA_X+190, LCD_WIDTH_PX+6, TABLA_Y, buffer, 1);
	gomb_kozepre(TABLA_X+190, LCD_WIDTH_PX+6, TABLA_Y+37, buffer2, 1);
}

void display_cell(int cx, int cy){
	VRAM_CopySprite(tiles[jatek.palya[cx][cy]], TABLA_X+6+cx*46, TABLA_Y+6+cy*46, 40, 40);
}

void display_grid(){
	int cx, cy;
	for(cx=0; cx<4; cx++){
	for(cy=0; cy<4; cy++){
		display_cell(cx, cy);
	}
	}
}

void decompress(){
	char decomp[57600];
	qlz_state_decompress *state_decompress = (qlz_state_decompress *)malloc(sizeof(qlz_state_decompress));
	qlz_decompress((char *) sprites_comp, decomp, state_decompress);
	free(sprites_comp);
	memcpy(tiles, decomp, 57600);
}

void forgat_sarok(unsigned short sarok2[8][8], char forg){ //ob: cél index, src: forrás index, forg: 0 fel, 1: jobb, 2: le, 3: bal
	int x, y;
	switch(forg){
		case 1:
				for(x=0;x<8;x++){
				for(y=0;y<8;y++){
					sarok2[y][x] = sarok[7-x][y];
				}}
			break;
		case 2:
				for(x=0;x<8;x++){
				for(y=0;y<8;y++){
					sarok2[y][x] = sarok[7-y][7-x];
				}}
			break;
		case 3:
				for(x=0;x<8;x++){
				for(y=0;y<8;y++){
					sarok2[y][x] = sarok[x][7-y];
				}}
			break;
		case 0://fel
		default:
				for(x=0;x<8;x++){
				for(y=0;y<8;y++){
					sarok2[x][y] = sarok[x][y];
				}}
			break;
	}
}

void sarkak(){
	VRAM_CopySprite(sarok, TABLA_X, TABLA_Y, 8, 8);
	unsigned short sarok2[8][8];
	forgat_sarok(sarok2, 1);
	VRAM_CopySprite(sarok2, TABLA_X+190-8, TABLA_Y, 8, 8);
	forgat_sarok(sarok2, 2);
	VRAM_CopySprite(sarok2, TABLA_X+190-8, TABLA_Y+190-8, 8, 8);
	forgat_sarok(sarok2, 3);
	VRAM_CopySprite(sarok2, TABLA_X, TABLA_Y+190-8, 8, 8);
}

void init_display(){
	Bdisp_EnableColor(1);
	EnableStatusArea(0);
	DefineStatusAreaFlags(DSA_SETDEFAULT, 0, 0, 0);
	DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);
	DefineStatusMessage("2048 by Balping",1,0,0);
	
	decompress();
	
	Bdisp_Fill_VRAM(SZIN_HATTER, 1);
	FrameColor(1, SZIN_HATTER);
	DrawFrame(SZIN_HATTER);
	fillArea(TABLA_X, TABLA_Y, 190, 190, SZIN_TABLA);
	sarkak();
	
	display_grid();
	display_score();
	
	
	gomb_kozepre(TABLA_X+190, LCD_WIDTH_PX+6, LCD_HEIGHT_PX-34-1*37, "[F1]: About", 0);
	//gomb_kozepre(TABLA_X+190, LCD_WIDTH_PX+6, LCD_HEIGHT_PX-34-1*37, "[F2]: Settings", 0);
	gomb_kozepre(TABLA_X+190, LCD_WIDTH_PX+6, LCD_HEIGHT_PX-34, "[F2]: New Game", 0);
}

void dialog_continue(){
	display_grid();
	display_score();
	MsgBoxPush(5);
	PrintXY(3, 2, "xxYou win!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(3, 3, "xxContinue?", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 5, 3, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 6, 4, TEXT_COLOR_BLACK);
	int key;
	char vissza=2;
	do{
		GetKey(&key);
		switch(key){
			case KEY_CTRL_F1:
				vissza = 1;
				break;
			case KEY_CTRL_F6:
				vissza = 0;
				break;
		}
	}while(vissza==2);
	if(!vissza){init_jatek();}
	MsgBoxPop();
	clear_score();
}

void dialog_vege(){
	display_grid();
	display_score();
	MsgBoxPush(3);
	PrintXY(3, 3, "xxGame over!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 5, 2, TEXT_COLOR_BLACK);
	int key=0;
	while(key!=KEY_CTRL_EXIT){
		GetKey(&key);
	}
	init_jatek();
	MsgBoxPop();
	clear_score();
}

char new_game_dialog(){
	MsgBoxPush(4);
	PrintXY(3, 2, "xxAre you sure?", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 4, 3, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 5, 4, TEXT_COLOR_BLACK);
	int key;
	char vissza=2;
	do{
		GetKey(&key);
		switch(key){
			case KEY_CTRL_F1:
				vissza = 1;
				break;
			case KEY_CTRL_F6:
			case KEY_CTRL_EXIT:
				vissza = 0;
				break;
		}
	}while(vissza==2);
	MsgBoxPop();
	return vissza;
}

